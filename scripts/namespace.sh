#!/bin/sh
set -e
namespace=$1
echo running: okteto namespace "$namespace"
okteto namespace "$namespace"
k="$HOME/.kube/config"
echo "::set-output name=kubeconfig::$k"