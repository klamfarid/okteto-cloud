#!/bin/sh
set -e

namespace=$1

wget -O okteto-namespace-master-dir.tar.gz  https://gitlab.com/klamfarid/okteto-namespace/-/archive/master/okteto-namespace-master.tar.gz 
tar -xzf okteto-namespace-master-dir.tar.gz
cd okteto-namespace-master
docker build --pull -t "$CI_REGISTRY_IMAGE"  .
docker push "$CI_REGISTRY_IMAGE"
docker run $CI_REGISTRY_IMAGE  $namespace