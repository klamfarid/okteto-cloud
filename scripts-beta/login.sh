#!/bin/sh
set -e

token=$1

if [ -z $token ]; then
  echo "Okteto API token is required to run login container "
  exit 1
fi

wget -O okteto-login-master-dir.tar.gz  https://gitlab.com/klamfarid/okteto-login/-/archive/master/okteto-login-master.tar.gz 
tar -xzf okteto-login-master-dir.tar.gz
cd okteto-login-master
docker build --pull -t "$CI_REGISTRY_IMAGE"  .
docker push "$CI_REGISTRY_IMAGE"
docker run $CI_REGISTRY_IMAGE  $token