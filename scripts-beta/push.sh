#!/bin/sh
set -e

namespace=$1
name=$2
deploy=$3
wd=$4

params=""

if [ ! -z "$namespace" ]; then
params="${params} --namespace $namespace"
fi

if [ ! -z "$name" ]; then
params="${params} --name $name"
fi

if [ "$deploy" == "true" ]; then
params="${params} --deploy"
fi

if [ ! -z "$wd" ]; then
cd $wd
fi


wget -O okteto-push-master-dir.tar.gz  https://gitlab.com/klamfarid/okteto-push/-/archive/master/okteto-push-master.tar.gz 
tar -xzf okteto-push-master-dir.tar.gz
cd okteto-push-master
docker build --pull -t "$CI_REGISTRY_IMAGE"  .
docker push "$CI_REGISTRY_IMAGE"
docker run $CI_REGISTRY_IMAGE  $params