#!/bin/sh
set -e

manifest=$1
namespace=$2
params=""
if [ ! -z $namespace ]; then
params="--namespace $namespace"
fi

wget -O okteto-apply-master-dir.tar.gz  https://gitlab.com/klamfarid/okteto-apply/-/archive/master/okteto-apply-master.tar.gz 
tar -xzf okteto-apply-master-dir.tar.gz
cd okteto-apply-master
docker build --pull -t "$CI_REGISTRY_IMAGE"  .
docker push "$CI_REGISTRY_IMAGE"
docker run $CI_REGISTRY_IMAGE  $manifest $params